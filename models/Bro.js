var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Bro Model
 * ==========
 */
var Bro = new keystone.List('Bro');

Bro.add({
	name: { type: Types.Name, required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, unique: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true },
});

// Provide access to Keystone
Bro.schema.virtual('canAccessKeystone').get(function () {
	return this.isAdmin;
});


/**
 * Relationships
 */
Bro.relationship({ ref: 'Post', path: 'posts', refPath: 'author' });


/**
 * Registration
 */
Bro.defaultColumns = 'name, email, isAdmin';
Bro.register();
